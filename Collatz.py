#!/usr/bin/env python3

# ----------
# Collatz.py
# ----------

# pylint: disable = bad-whitespace
# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------

from typing import IO, List

# ------------
# collatz_read
# ------------


def collatz_read(s: str) -> List[int]:
    """
    read two ints
    s a string
    return a list of two ints, representing the beginning and end of a range, [i, j]
    """
    a = s.split()
    return [int(a[0]), int(a[1])]


# ------------
# collatz_eval
# ------------

# cache to store computed results
cache = [0] * 10000000
cache[1] = 1


def collatz_eval(i: int, j: int) -> int:
    """
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    return the max cycle length of the range [i, j]
    """
    # <your code>
    assert 0 < i < 1000000 and  0 < j < 1000000
    if i > j:
        i, j = j, i
    assert i <= j
    # reduced the range
    if i < j / 2 + 1:
        i = (int)(j / 2) + 1
    mx_idx = max(cycle_length(t) for t in range(i, j + 1))
    assert mx_idx > 0
    return mx_idx


# ------------
# cycle_length
# ------------


def cycle_length(i: int) -> int:
    """
    return the cycle length of i
    """
    assert i > 0
    # first check if it is stored in cache
    if i < 10000000 and cache[i] != 0:
        return cache[i]
    if i % 2 == 0:
        cur_cl = 1 + cycle_length((int)(i / 2))
    else:
        cur_cl = 2 + cycle_length((int)((i * 3 + 1) / 2))
    if i < 10000000:
        cache[i] = cur_cl
    assert cur_cl > 0
    return cur_cl


# -------------
# collatz_print
# -------------


def collatz_print(w: IO[str], i: int, j: int, v: int) -> None:
    """
    print three ints
    w a writer
    i the beginning of the range, inclusive
    j the end       of the range, inclusive
    v the max cycle length
    """
    w.write(str(i) + " " + str(j) + " " + str(v) + "\n")


# -------------
# collatz_solve
# -------------


def collatz_solve(r: IO[str], w: IO[str]) -> None:
    """
    r a reader
    w a writer
    """
    for s in r:
        i, j = collatz_read(s)
        v = collatz_eval(i, j)
        collatz_print(w, i, j, v)
