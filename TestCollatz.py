#!/usr/bin/env python3

# --------------
# TestCollatz.py
# --------------

# pylint: disable = bad-whitespace
# pylint: disable = invalid-name
# pylint: disable = missing-docstring

# -------
# imports
# -------

from io import StringIO
from unittest import main, TestCase

from Collatz import collatz_read, collatz_eval, collatz_print, collatz_solve

# -----------
# TestCollatz
# -----------
class TestCollatz(TestCase):
    # ----
    # read
    # ----

    def test_read(self):
        s = "3 11\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 3)
        self.assertEqual(j, 11)

    def test_read_1(self):
        s = "52 21\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 52)
        self.assertEqual(j, 21)

    def test_read_2(self):
        s = "100 200\n"
        i, j = collatz_read(s)
        self.assertEqual(i, 100)
        self.assertEqual(j, 200)

    # ----
    # eval
    # ----

    def test_eval_1(self):
        v = collatz_eval(4307, 7160)
        self.assertEqual(v, 262)

    def test_eval_2(self):
        v = collatz_eval(7112, 6008)
        self.assertEqual(v, 262)

    def test_eval_3(self):
        v = collatz_eval(7900, 7176)
        self.assertEqual(v, 239)

    def test_eval_4(self):
        v = collatz_eval(7112, 6008)
        self.assertEqual(v, 262)

    # -----
    # print
    # -----

    def test_print(self):
        w = StringIO()
        collatz_print(w, 4307, 7160, 262)
        self.assertEqual(w.getvalue(), "4307 7160 262\n")

    def test_print_1(self):
        w = StringIO()
        collatz_print(w, 7112, 6008, 262)
        self.assertEqual(w.getvalue(), "7112 6008 262\n")

    # -----
    # solve
    # -----

    def test_solve(self):
        r = StringIO("4307 7160\n7112 6008\n")
        w = StringIO()
        collatz_solve(r, w)
        self.assertEqual(w.getvalue(), "4307 7160 262\n7112 6008 262\n")


# ----
# main
# ----

if __name__ == "__main__":  # pragma: no cover
    main()
