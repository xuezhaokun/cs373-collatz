#!/usr/bin/env python3
from sys import stdin, stdout
import random
# ----
# main
# ----

def gen_acceptancetest (w):
    for ind in range(0, 200):
        i = random.randint(1, 10000)
        j = random.randint(1, 10000)
        w.write(str(i) + " " + str(j)+ "\n")

if __name__ == "__main__" :
    gen_acceptancetest(stdout)